# Compiling and installing OpenCV 3.4.3 + Contrib modules for python 3.5 on DragonBoard

This tutorial presents the process of compiling the OpenCV 3.4.3 libraries and the Contrib modules for Python version 3.5 on DragonBoard410c. The current version already has a script that performs all procedures automatically.

**Important**

The first step is to mount a swap memory area so that the installations occur without errors. To do this, follow [this tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.md), with the step-by-step procedure.

Before moving on, make sure the system date is up to date. On the terminal run the command:

```
date
```

> If the date is wrong, run the command **date -s “DD/MM/YYYY HH:MM:SS”**. For example:
> > sudo date –s “12/04/2018 10:08:00”

With the swap space enabled, the correct date and the card mounted in the **media/sdcard** directory, download the repository containing the script and execute with the following commands:

```sh

git clone https://gitlab.com/qualcomm-iot-reference-center/utilidades.git

cd utilidades

sudo sh opencv3-installer.sh

cd ..

rm -r utilidades
```


# Script description - step by step

## Basic Requirements (Updates and Libraries)

Initially, it is necessary to install some libraries and basic modules to compile the cv2 lib. To do this, the script executes the following commands on the board terminal:

```
sudo apt-get update
sudo apt-mark hold linux-image-4.14.0-qcomlt-arm64
sudo apt-get dist-upgrade
sudo apt-get install build-essential cmake pkg-config -y
sudo apt-get install libjpeg-dev libtiff5-dev libpng-dev -y
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt-get install libxvidcore-dev libx264-dev -y
sudo apt-get install libgtk2.0-dev libgtk-3-dev -y
sudo apt-get install libatlas-base-dev gfortran -y
sudo apt-get install hdf5* libhdf5*
sudo apt-get install v4l-utils
```

## Python3.5 Installation
The script performs the procedures described [in this tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/aiFrameworks.md) to install version 3.5 of Python, requirement for compilation.

## Compilation process

The library codes will now be downloaded and the compilation process executed. The commands executed by the script are:


```
sudo git clone -b 3.4 https://github.com/opencv/opencv.git
sudo git clone -b 3.4 https://github.com/opencv/opencv_contrib.git
# Instale o numpy
sudo python3.5 -m pip install --upgrade pip
sudo python3.5 -m pip install --upgrade setuptools
sudo python3.5 -m pip install numpy
cd opencv
sudo mkdir build
cd build

sudo cmake -D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_INSTALL_PREFIX=/usr/local \
-D BUILD_opencv_java=OFF \
-D BUILD_opencv_python2=OFF \
-D BUILD_opencv_python3=ON \
-D PYTHON_DEFAULT_EXECUTABLE=$(which python3.5) \
-D INSTALL_C_EXAMPLES=OFF \
-D INSTALL_PYTHON_EXAMPLES=OFF \
-D BUILD_EXAMPLES=OFF\
-D WITH_CUDA=OFF \
-D BUILD_TESTS=OFF \
-D ENABLE_NEON=ON \
-D BUILD_PERF_TESTS= OFF \
-D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules .. #altere o diretório para os módulos contrib caso necessário

sudo make -j 1
sudo make install

export PYTHONPATH="${PYTHONPATH}:/usr/local/python/cv2/python-3.5/"
```

# Installation test

To test the OpenCV installation, access the terminal and run python3.5 as indicated below:

```sh
python3.5
>>> import cv2
>>> cv2.__version__
>>> exit()
```

The response from this command is shown below:
<div align= "center">
    <figure>
        <img src = "/fotos_opencv/fig1.png"/>
        <figcaption>Validação da instalação do OpenCV</figcaption>
    </figure>
</div>