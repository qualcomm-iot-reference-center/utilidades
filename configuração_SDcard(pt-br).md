# Configuração do cartão microSD para extensão do File System e Swap

Este tutorial descreve os procedimentos necessários para configurar um cartão SD como extensão do sistema de arquivos e área de memória SWAP.

## Introdução

Sistemas embarcados como a DragonBoard 410c possuem recursos mais limitados em comparação ao hardware de computadores atuais. Uma das limitações que podem comprometer a execução de projetos, mas que pode ser resolvida de maneira simples, é a pouca quantidade de memória RAM.

A forma de resolver a limitação é uma técnica utilizada até em computadores convencionais para garantir o funcionamento do sistema operacional, a SWAP Memory. A ideia é alocar um espaço da memória do disco para ser utilizada como RAM em caso de necessidade. A velocidade de operação de um HD é mais lenta, por isso a área de SWAP é utilizada apenas para operações menos críticas, ou para evitar o estouro de memória durante a execução de alguma tarefa.

Neste tutorial, vamos utilizar um cartão SD como memória SWAP. Para sistemas embarcados, ele possui uma boa velocidade de operação, e resolve a problema de executar tarefas mais custosas, como compilar bibliotecas pesadas ou rodar códigos complexos que exigem muita RAM.

Uma última observação, como utilizar toda a capacidade de um cartão SD moderno como SWAP seria exagero, nossa sugestão é escolher uma fração da capacidade para o SWAP, e o restante ser utilizado como extensão do file system, aumentando também a capacidade de armazenamento do seu sistema.

## Formatação do cartão SD

> **Importante** Dimensione o cartão SD de modo que a memória de SWAP tenha ao menos 1,5 GB.

**Passo 1:** Execute o comando **lsblk** no terminal da placa para listar os devices conectados.

Insira o cartão na DB410C e execute o comando novamente para identificar como o novo dispositivo foi reconhecido pelo sistema. 

<div align= "center">
    <figure>
        <img src = "/fotos_cartaoSD/fig1.png"/>
        <figcaption>Exibição de devices conectados</figcaption>
    </figure>
</div>

**Passo 2:** No terminal da DB410C, execute os comandos indicados a seguir. Ao final do procedimento o cartão será formatado para o padrão FAT32.
- Acesse o fdisk indicando o dispositivo que será formatado:
```
sudo fdisk /dev/mmcblk1
```
- Para criar a partição primária, execute os comandos:
    1. Deletar partições: d
    2. Criar uma nova partição: n
    3. Definir como partição primária: p
    4. Definir ID da partição como 1: 1
    5. Pressione ENTER para configurar o First Sector com tamanho padrão
    6. Defina o Last Sector de acordo com o cálculo efetuado para o particionamento, por exemplo: 93551615
   
**No sexto passo, o Last Sector depende do tamanho desejado para a partição**. Para definir o valor, encontre a partição desejada da memória total. Nosso cartão possui 124735487 endereços e desejamos utilizar 3/4 da capacidade na primeira partição, o que corresponde à 93551615.
 
<div align= "center">
    <figure>
        <img src = "/fotos_cartaoSD/fig2.png"/>
        <figcaption>Criação das partições</figcaption>
    </figure>
</div>

- Para criar a região de Swap, execute os comandos:
    1. Criar uma nova partição: n
    2. Definir como partição primária: p
    3. Definir ID da partição como 2: 2
    4. Pressione ENTER para configurar o Fisrt Sector com tamanho padrão
    5. Pressione ENTER para configurar o Last Sector com tamanho padrão

**Como o tamanho foi definido como padrão, todo o restante da capacidade do cartão vai ser definido como a partição 2 neste exemplo.**

- Para configurar o tipo das partições, execute os comandos:
    1. Alterar o tipo da partição: t
    2. Indicar ID da partição: 1
    3. Indicar o tipo da partição: digite **L** para listar todas as partições possíveis, e encontre o número correspondente à **Linux filesystem**. Digite o número 
    4. Alterar o tipo da partição: t
    5. Indicar ID da partição: 2
    6. Indicar o tipo da partição de SWAP: digite **L** para listar todas as partições possíveis, e encontre o número correspondente à **Linux swap**. Digite o número 
    7. Exibir a tabela de partições: p
    8. Escrever as alterações e sair do fdisk: w

## Formatação do cartão SD
Após a configuração do microSD, as partições devem ser inicializadas na DB410C. Para tal, habilite o File System com os seguintes comandos no terminal:
```
# Formata a partição criada para posterior utilização
sudo mkfs.ext3 /dev/mmcblk1p1
 
# Cria o diretório sdcard em media
sudo mkdir /media/sdcard 
 
# Monta o partição do tipo Linux no diretório sdcard
sudo mount -t ext3 /dev/mmcblk1p1 /media/sdcard
```

Em seguida, habilite a área de SWAP:
```
# Habilita a partição do tipo SWAP para utilização
sudo mkswap /dev/mmcblk1p2
sudo swapon /dev/mmcblk1p2
 
# Verifica o status da partição de SWAP
sudo swapon -s
```

<div align= "center">
    <figure>
        <img src = "/fotos_cartaoSD/fig2.png"/>
        <figcaption>Ativação das partições criadas</figcaption>
    </figure>
</div>

## Scripts para inicialização

O procedimento realizado (Montagem da área de SWAP e do *File System*) pode ser configurado para ser executado automaticamente durante a inicialização do sistema. Nesse caso, o crontab pode ser utilizado para executar um script.

- Crie o diretório de scripts na home do seu usuário:
```
mkdir /home/$USER/scripts
```

- Crie um script e indique os comandos que serão executados para montagem das partições:
```
cd /home/$USER/scripts
sudo touch inicializacao.sh
sudo chmod 700 inicializacao.sh
echo "sudo mount -t ext3 /dev/mmcblk1p1 /media/sdcard" >> inicializacao.sh
echo "sudo swapon /dev/mmcblk1p2" >> inicializacao.sh
```

- Execute o crontab e programe o gatilho para execução do script:
```
sudo crontab -e
```

Escolha o editor de sua preferência e no final do arquivo copie o seguinte comando:
```
@reboot /home/<seu_usuário>/scripts/inicializacao.sh
```

**Obs.:** Substitua <seu_usuário> pelo seu nome do usuário onde foi criado o diretório de scripts

**Agora o script será executado durante o procedimento de inicialização.**