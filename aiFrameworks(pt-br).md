# Instalação de frameworks para Machine Learning e Deep Learning: Tensorflow, Dlib, Sci-kit learn

## Guia de configuração para utilização de frameworks de Machine Learning e Deep Learning na DragonBoard 410c, desenvolvido pelo IoT Referencce Center, parceria entre Qualcomm e FACENS.

Nesse guia são listadas as dependências e comandos necessários para instalar o Python 3.5, Tensorflow 1.11, Scikit-learn e Dlib em uma placa DragonBoard 410c.

Todos os comandos devem ser executados na DragonBoard, não é necessário uma máquina extra.

## Introdução

Com o grande crescimento recente em pesquisa e em aplicação de Inteligência Artificial, várias ferramentas de desenvolvimento surgiram para facilitar o trabalho do desenvolvedor. Diversas empresas e iniciativas open source desenvolveram frameworks que agilizam o trabalho com dados, operações matemáticas complexas e manipulação de tensores. Neste tutorial, instalaremos 3 ferramentas no sistema embarcado da DragonBoard, possibilitando a inferência de IA na borda.

## Tensorflow

É um framework open-source de Machine Learning usado para o treinamento de redes neurais. Nascida nos laboratórios da Google, com a biblioteca é possível criar aplicações de inteligência artificial para decifrar padrões e fazer correlações entre dados. As operações são construídas usando gráficos de fluxo de dados (Tensors).

## Scikit-learn

É uma biblioteca open-source de Machine Learning que oferece ferramentas para mineração e análise de dados. Inclui algoritmos de classificação, regressão e clustering e é construída com base nas bibliotecas numéricas e científicas do Python: Numpy e SciPy.

## Dlib

É uma biblioteca de software multi-plataforma criada em C++, englobando algoritmos que auxiliam em desenvolvimentos relacionados a machine learning (Modelos convencionais e Deep Learning), operações numéricas (Operações convencionais como derivadas, direcionadas também a cálculos vetoriais), processamento de imagem, e diversas outras ferramentas. Para saber mais, acesse a página da biblioteca [aqui](http://dlib.net/).

# Passo 1 - Dependências

## Instalando Python 3.5

Como dependência, o TensorFlow 1.11 precisa da versão 3.5.x do python instalada para seu funcionamento (No guia aqui apresentado, instalaremos a 3.5.6). Para tal, siga os passos abaixo no terminal da DB410c:

1. Execute os comandos abaixo para instalar as dependências da instalação do Python3:
```
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
```
2. O próximo comando realiza o download do código fonte do Python3.5:
```
cd /usr/src
sudo wget https://www.python.org/ftp/python/3.5.6/Python-3.5.6.tgz
```
3. Extraia o arquivo compactado e entre no diretório dos arquivos extraídos para que o source code possa ser compilado:
```
sudo tar xzf Python-3.5.6.tgz
sudo rm Python-3.5.6.tgz
cd Python-3.5.6
```
4. Use o seguinte comando para verificar as configurações do seu ambiente e também verificar se o Python pode ser instalado, gerando as configurações de instalação:
```
sudo ./configure --enable-optimizations
```
5. Após executado o último comando, deverá haver um arquivo Makefile no diretório. Para fazer a verificação, liste o conteúdo da pasta atual usando o comando **ls**.

6. Instale o source code usando o comando **make**:
```
sudo make altinstall
```
> O parâmetro altinstall evita a reposição da versão padrão do Python já instalada.

7. Verifique a versão do Python após a instalação:
```
python3.5 -V
>> 3.5.6
```
8. Atualize o gerenciador de pacotes do Python, o Pip:
```
sudo python3.5 -m pip install --upgrade pip
```

## Instalando dependências adicionais (Bibliotecas, compilador)

Instale as dependências adicionais com o comando abaixo:

```
sudo apt-get install cmake libgtk2.0-dev libgtk-3-dev libhdf5-dev libatlas-base-dev gfortran python3-grpcio –y
```

# Passo 2 - Instalação dos frameworks

## Instalando TensorFlow

**Dependência:** Para a instalação do TensorFlow, alguns requisitos devem ser atendidos:
- Configurar uma área de troca **(SWAP)**. Siga o guia [aqui](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.md).
- Desabilite o mecanismo de economia de energia, para que a taxa de download não seja limitada pelo sistema:

```
sudo iwconfig wlan0 power off
```

- Configurar a data. Para isto use o comando abaixo, alterando a hora para o seu caso (Padrão a seguir: “mês/dia/ano h:min:sec”): 

```
sudo date –s “11/30/2018 15:30:00”
```

- Os compiladores gcc e g++ devem estar na mesma versão (8.2). Para tal, execute o seguinte comando:

```
sudo apt-get install g++-8
sudo apt-get install gcc-8
```

1. Baixe o wheel do Tensorflow do repositório do GitHub, em um diretório a sua escolha na placa (pasta Downloads, por exemplo):

### repositório com binários para qualquer versão do python: https://github.com/PINTO0309/Tensorflow-bin

```
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.11.0/tensorflow-1.11.0-cp35-none-linux_aarch64.whl
```

2. Utilize o pip para a instalação:

```
sudo python3.5 -m pip install tensorflow-1.11.0-cp35-none-linux_aarch64.whl
```

3. Confira a instalação pelo python:


```
python3.5
>> import tensorflow
>> exit()
```

Caso não ocorra nenhuma mensagem de erro, o TensorFlow foi instalado com sucesso. Para remover o wheel execute no terminal: 

```
rm tensorflow-1.11.0-cp35-none-linux_aarch64.whl
```

## Instalando Scikit-learn

> A instalação das próximas bibliotecas, Scikit e Dlib, também requer a utilização de uma área de Swap, então não desative seu cartão SD.


1. Utilizando o pip, instale a biblioteca Scikit-learn. É um processo que demanda certo tempo: aproximadamente 3 horas.


```
sudo python3.5 -m pip install scikit-learn
```

2. Verifique a instalação:


```
python3.5
>> import sklearn
>> exit()
```

> A biblioteca é referenciada como 'sklearn' pelo Python.

## Instalando Dlib


1. Através do pip, instale a biblioteca Dlib. É um processo que demanda certo tempo: aproximadamente 3 horas.

```
sudo python3.5 -m pip install dlib
```

Verifique se a biblioteca foi instalada corretamente:
```
python3.5
import dlib
exit()
```