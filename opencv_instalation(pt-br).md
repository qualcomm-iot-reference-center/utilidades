# Compilação e instalação do OpenCV 3.4.3 + Contrib modules para python 3.5 na DragonBoard

Este tutorial apresenta o processo de compilação das bibliotecas OpenCV 3.4.3 e dos módulos Contrib para a versão 3.5 do Python na DragonBoard410c. A versão atual já possui um script que realiza todos os procedimentos automaticamente.

# Script de instalação


**Importante** 

O primeiro passo é montar uma área de SWAP de memória para que as instalações ocorram sem erros. Para isso, siga [este tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.md), com o passo a passo do procedimento.

Antes de seguir em frente, verifique se a data do sistema está atualizada. No terminal execute o comando:

```
date
```

> Se a data estiver errada, execute o comando **date -s “DD/MM/YYYY HH:MM:SS”**. Por exemplo:
> > sudo date –s “12/04/2018 10:08:00”

Com o swap ativado, a data correta e o cartão montado no diretório **media/sdcard**, faça o download do repositório que contém o script e execute com os comandos a seguir:

```sh

git clone https://gitlab.com/qualcomm-iot-reference-center/utilidades.git

cd utilidades

sudo sh opencv3-installer.sh

cd ..

rm -r utilidades
```


# Descritivo do script - passo a passo

## Requisitos básicos (Atualizações e Bibliotecas)

Inicialmente, é necessário instalar algumas bibliotecas e módulos básicos para fazer a compilação. Para tal, o script executa os seguintes comandos no terminal da DragonBoard:

```
sudo apt-get update
sudo apt-mark hold linux-image-4.14.0-qcomlt-arm64
sudo apt-get dist-upgrade
sudo apt-get install build-essential cmake pkg-config -y
sudo apt-get install libjpeg-dev libtiff5-dev libpng-dev -y
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt-get install libxvidcore-dev libx264-dev -y
sudo apt-get install libgtk2.0-dev libgtk-3-dev -y
sudo apt-get install libatlas-base-dev gfortran -y
sudo apt-get install hdf5* libhdf5*
sudo apt-get install v4l-utils
```

## Instalação Python3.5
O script realiza os procedimentos descritos [neste tutorial](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/aiFrameworks.md) para instalar a versão 3.5 do Python, requisito para a compilação.

## Processo de compilação

Agora serão baixados os códigos da biblioteca e executado o processo de compilação. Os comandos executados pelo script são:


```
sudo git clone -b 3.4 https://github.com/opencv/opencv.git
sudo git clone -b 3.4 https://github.com/opencv/opencv_contrib.git
# Instale o numpy
sudo python3.5 -m pip install --upgrade pip
sudo python3.5 -m pip install --upgrade setuptools
sudo python3.5 -m pip install numpy
cd opencv
sudo mkdir build
cd build

sudo cmake -D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_INSTALL_PREFIX=/usr/local \
-D BUILD_opencv_java=OFF \
-D BUILD_opencv_python2=OFF \
-D BUILD_opencv_python3=ON \
-D PYTHON_DEFAULT_EXECUTABLE=$(which python3.5) \
-D INSTALL_C_EXAMPLES=OFF \
-D INSTALL_PYTHON_EXAMPLES=OFF \
-D BUILD_EXAMPLES=OFF\
-D WITH_CUDA=OFF \
-D BUILD_TESTS=OFF \
-D ENABLE_NEON=ON \
-D BUILD_PERF_TESTS= OFF \
-D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules .. #altere o diretório para os módulos contrib caso necessário

sudo make -j 1
sudo make install

export PYTHONPATH="${PYTHONPATH}:/usr/local/python/cv2/python-3.5/"
```

# Testando a instalação

Para testar a instalação do OpenCV, acesse o terminal e execute o python3.5 conforme indicado abaixo:

```sh
python3.5
>>> import cv2
>>> cv2.__version__
>>> exit()
```

A resposta deste comando é mostrada abaixo:
<div align= "center">
    <figure>
        <img src = "/fotos_opencv/fig1.png"/>
        <figcaption>Validação da instalação do OpenCV</figcaption>
    </figure>
</div>