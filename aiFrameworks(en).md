# Installation of Machine Learning and Deep Learning frameworks: Tensorflow, Dlib, Sci-kit learn

## Configuration Guide for use of Machine Learning and Deep Learning frameworks in the DragonBoard 410c, developed by the IoT Referencce Center, a partnership between Qualcomm and FACENS.

This tab lists the dependencies and commands required to install Python 3.5, Tensorflow 1.11, Scikit-learn and Dlib on a DragonBoard 410c card.

All commands must be executed in DragonBoard, no extra machine is required.

## Introduction

With the recent growth in research and application of Artificial Intelligence, several development tools have emerged to facilitate developers work. Several companies and open source initiatives have developed frameworks that streamline data work, complex mathematical operations, and tensor manipulation. In this tutorial, we will install 3 tools in the embedded system DragonBoard, allowing the inference of AI in the edge.

## Tensorflow

It is an open-source framework of Machine Learning used for the training of neural networks. Born in Google's labs, with the library it's possible to create artificial intelligence applications to decipher patterns and make correlations between data. The operations are constructed using data flow graphs (Tensors).

## Scikit-learn

It is an open source library of Machine Learning that offers tools for mining and data analysis. It includes classification, regression and clustering algorithms and is built on the basis of the numerical and scientific libraries of Python: Numpy and SciPy.

## Dlib

It is a multi-platform software library created in C ++, encompassing algorithms that aid in developments related to machine learning (conventional models and deep learning), numerical operations (conventional operations as derivatives, also directed to vector calculations), image processing, and many other tools. To learn more, visit the library [page](http://dlib.net/).

# Passo 1 - Dependências

## Installing Python 3.5

As a dependency, TensorFlow 1.11 needs the version 3.5.x of python installed for its operation (In the guide presented here, we will install 3.5.6). To do this, follow the steps below on the DB410c terminal:

1. Run the commands below to install the dependencies for the Python3 installation:
```
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
```
2. The next command downloads the Python3.5 source code:
```
cd /usr/src
sudo wget https://www.python.org/ftp/python/3.5.6/Python-3.5.6.tgz
```
3. Extract the compressed file and enter the directory of the extracted files so that the source code can be compiled:
```
sudo tar xzf Python-3.5.6.tgz
sudo rm Python-3.5.6.tgz
cd Python-3.5.6
```
4. Use the following command to check your environment settings and also verify that Python can be installed, generating the installation settings:
```
sudo ./configure --enable-optimizations
```
5. After the last command run, there should be a Makefile in the directory. To do the scan, list the contents of the current folder using the **ls** command.

6. Install the source code using the **make** command:
```
sudo make altinstall
```
> The altinstall parameter avoids replacing the default version of Python already installed.

7. Check the Python version after installation:
```
python3.5 -V
>> 3.5.6
```
8. Update the Python package manager, Pip:
```
sudo python3.5 -m pip install --upgrade pip
```

## Installing additional dependencies (Libraries, compiler)

Install additional dependencies with the command below:

```
sudo apt-get install cmake libgtk2.0-dev libgtk-3-dev libhdf5-dev libatlas-base-dev gfortran python3-grpcio –y
```

# Step 2 - Installing the frameworks

## Installing TensorFlow

**Dependency:** For the installation of TensorFlow, some requirements must be met:
- Set up a swap memory area. Follow the guide [here](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard.md).
- Disable the power saving tool so that the download rate is not limited by the system:

```
sudo iwconfig wlan0 power off
```

- Set the date. To do this, use the command below, changing the time for your case (Default is as follows: "month/day/year h:min:sec"): 

```
sudo date –s “11/30/2018 15:30:00”
```

- Gcc e G++ compilers must be in the same version (8.2). To do this, run the following command:

```
sudo apt-get install g++-8
sudo apt-get install gcc-8
```

1. Download the Tensorflow wheel from the GitHub repository, in a directory of your choice on the board (Downloads folder, for example):

```
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.11.0/tensorflow-1.11.0-cp35-none-linux_aarch64.whl
```

2. Use Pip for installation:

```
sudo python3.5 -m pip install tensorflow-1.11.0-cp35-none-linux_aarch64.whl
```

3. Check installation with python:


```
python3.5
>> import tensorflow
>> exit()
```

If no error message occurs, TensorFlow has been successfully installed. To remove the wheel run in the terminal: 

```
rm tensorflow-1.11.0-cp35-none-linux_aarch64.whl
```

## Installing Scikit-learn

> Installing the next libraries, Scikit and Dlib, also requires the use of a swap area, so do not disable your SD card.


1. Using pip, install the Scikit-learn library. This is a time-consuming process: approximately 3 hours.


```
sudo python3.5 -m pip install scikit-learn
```

2. Verify the installation:


```
python3.5
>> import sklearn
>> exit()
```

> The library is referenced as 'sklearn' in Python.

## Instalando Dlib


1. Using Pip, install the Dlib library. This is a time-consuming process: approximately 3 hours.

```
sudo python3.5 -m pip install dlib
```

Make sure the library has been installed correctly:
```
python3.5
import dlib
exit()
```