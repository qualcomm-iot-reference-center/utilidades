# General guides for Dragonboard 410c development
## Guias de desenvolvimentos gerais para a Dragonboard 410c


## Installation of artificial intelligence frameworks
[English](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/aiFrameworks(en).md)
[Português](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/aiFrameworks(pt-br).md)

## Swap memory and SD card configuration
[English](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard(en).md)
[Português](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/configura%C3%A7%C3%A3o_SDcard(pt-br).md)

## OpenCV build from source
[English](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/opencv_installation(en).md)
[Português](https://gitlab.com/qualcomm-iot-reference-center/utilidades/blob/master/opencv_instalation(pt-br).md)