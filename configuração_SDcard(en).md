# Configuration of microSD card for File System and Swap extension

This tutorial describes the procedures required to configure an SD card as an extension of the file system and swap area.

## Introduction

Embedded systems like the DragonBoard 410c have more limited features compared to the current computer hardware. One of the limitations that can compromise the execution of projects, but that can be solved in a simple way, is the little amount of RAM.

The way to solve the limitation is a technique used even in conventional computers to ensure the operation of the OS, SWAP Memory. The idea is to allocate a disk space to be used as RAM in case of need. The work speed of a hard drive is slower, so the SWAP area is only used for less critical operations, or to avoid memory overflow during the execution of some task.

In this tutorial, we will use an SD card as SWAP memory. For embedded systems, it has a good work speed, and solves the problem of performing more expensive tasks, such as compiling heavy libraries or running complex codes that require lots of RAM.

For a better use of the SD card, we will use the rest of the capacity as an extension of the filesystem, also increasing the storage power of your system.

## Card configuration

** Important ** The SD card must be dimensioned so that the SWAP memory is at least 1.5 GB.

**Step 1:** Run the **lsblk** command on the board's terminal to list the connected devices.

Insert the card into the DB410C and run the command again to identify how the new device was recognized by the system.

<div align= "center">
    <figure>
        <img src = "/fotos_cartaoSD/fig1.png"/>
        <figcaption>Display of connected devices.</figcaption>
    </figure>
</div>

**Step 2:** At the DB410C terminal, execute the following commands. At the end of the procedure, the card will be formatted to the FAT32 standard.
- Access the fdisk indicating the device that will be formatted:

```
sudo fdisk /dev/mmcblk1
```

- To create the primary partition, run the commands:
    1. Delete partitions: d
    2. Create a new partition: n
    3. Define as primary partition: p
    4. Set partition ID to 1: 1
    5. Press ENTER to set up the First Sector with standard size
    6. Define the last sector according to the calculation made for the partitioning, **E.g .,** 93551615
   
**In the sixth step, the Last Sector depends on the size desired for the partition**. To set the value, find the desired partition in the total memory. Our card has 124735487 addresses and we want to use 3/4 of the capacity on the first partition, which corresponds to 93551615.


<div align= "center">
    <figure>
        <img src = "/fotos_cartaoSD/fig2.png"/>
        <figcaption>Partition creation.</figcaption>
    </figure>
</div>

- To create the Swap region, run the commands:
    1. Create a new partition: n
    2. Define as the primary partition: p
    3. Set partition ID to 2: 2
    4. Press ENTER to set up First Sector with standard size
    5. Press ENTER to set the Last Sector to the default size

**Because the size has been set as the default, all of the remaining capacity of the card will be set to partition 2 in this example.**

- To configure the partition type, run the commands:
    1. Change the partition type: t
    2. Enter Partition ID: 1
    3. Enter the type of filesystem partition: type **L** to list all possible partitions, and find the number corresponding to **Linux filesystem**. Enter the number
    4. Change the partition type: t
    5. Enter Partition ID: 2
    6. Specify the type of SWAP partition: type **L** to list all possible partitions, and find the number corresponding to **Linux swap**. Enter the number
    7. Display the partition table: p
    8. Write the changes and exit fdisk: w

## SD Card Formatting
After configuring the microSD, the partitions must be initialized in the DB410C. To do this, enable the File System with the following commands in the terminal:

```
# Formats the partition created for later use
sudo mkfs.ext3 /dev/mmcblk1p1
 
# Create the sdcard directory on the media
sudo mkdir /media/sdcard
 
# Mount the Linux partition in the sdcard directory
sudo mount -t ext3 /dev/mmcblk1p1 /media/sdcard
```

Then enable the SWAP area:

```
# Enable SWAP partition for use
sudo mkswap /dev/mmcblk1p2
sudo swapon /dev/mmcblk1p2

# Check the status of the SWAP partition
sudo swapon -s
```

<div align= "center">
    <figure>
        <img src = "/fotos_cartaoSD/fig2.png"/>
        <figcaption>Enable partitions created.</figcaption>
    </figure>
</div>


The procedure performed (Assembly of the SWAP area and the *File System*) can be configured to run automatically during system startup. In this case, the crontab can be used to run a script.

- Create the scripts directory in your user's home:

```
mkdir /home/$USER/scripts
```


- Create a script and indicate the commands that will be executed to mount the partitions:

```
cd /home/$USER/scripts
sudo touch inicializacao.sh
sudo chmod 700 inicializacao.sh
echo "sudo mount -t ext3 /dev/mmcblk1p1 /media/sdcard" >> inicializacao.sh
echo "sudo swapon /dev/mmcblk1p2" >> inicializacao.sh
```


- Run the crontab and schedule the trigger to run the script:

```
sudo crontab -e
```

- Choose the editor of your choice and at the end of the file copy the following command:

```
@reboot /home/<seu_usuário>/scripts/inicializacao.sh
```

**Note:** Replace <your_user> with your username where the scripts directory was created

**The script will now run on system startup.**