#! /bin/bash

echo '--- A PARTIÇÃO DO CARTÃO SD COMO EXTENSÃO DO FYLE SYSTEM ESTA EM MONTADA EM /media/sdcard? ---' 
echo -n '[y/n]:'
read option1

echo '--- A PARTIÇÃO DO CARTÃO SD CONFIGURADA COMO ÁREA DE TROCA (SWAP) ESTÁ ATIVADA? ---'
echo -n '[y/n]:'
read option2

echo "$option1"
echo "$option2"

if [ "${option1,}" = "n" ] && [ "${option2,}" = "n" ]
then
    echo '--- POR FAVOR, MONTE A PARTIÇÃO DO CARTÃO SD EM /media/sdcard, ATIVE A ÁREA DE TROCA (SWAP), E EXECUTE O SCRIPT NOVAMENTE ---' 
elif [ "${option1,}" = "n" ]
then
    echo '--- POR FAVOR, MONTE A PARTIÇÃO DO CARTÃO SD EM /media/sdcard E EXECUTE O SCRIPT NOVAMENTE ---'
elif [ "${option2,}" = "n" ]
then
    echo '--- POR FAVOR, ATIVE A ÁREA DE TROCA (SWAP) E EXECUTE O SCRIPT NOVAMENTE ---'
elif [ "${option1,}" = "y" ] && [ "${option2,}" = "y" ] 
then
    #echo 'Defina o python'
    echo '--- AS INSTALAÇÕES NECESSÁRIAS SERÃO EXECUTADAS ---'
    
    echo '--- ATUALIZANDO/INSTALANDO OS PACOTES/PROGRAMAS DISPONÍVEIS ---'
    sudo iwconfig wlan0 power off
    sudo apt-get update
    
    echo '--- ATUALIZANDO O SISTEMA ---'
    sudo apt-mark hold linux-image-4.14.0-qcomlt-arm64
    sudo apt-get dist-upgrade
    
    echo '--- INSTALANDO BIBLIOTECAS NECESSARIAS PARA INSTALAR O OPENCV ---'
    sudo apt-get install build-essential cmake pkg-config -y
    sudo apt-get install libjpeg-dev libtiff5-dev libpng-dev -y
    sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
    sudo apt-get install libxvidcore-dev libx264-dev -y
    sudo apt-get install libgtk2.0-dev libgtk-3-dev -y
    sudo apt-get install libatlas-base-dev gfortran -y
    sudo apt-get install hdf5* libhdf5*
    sudo apt-get install v4l-utils
    
    echo '--- INSTALANDO BIBLIOTECAS NECESSARIAS PARA INSTALAR O PYTHON 3.5 ---'
    sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
    
    echo '--- INSTALANDO PYTHON3.5 ---'
    cd /usr/src
    sudo wget https://www.python.org/ftp/python/3.5.6/Python-3.5.6.tgz
    sudo tar xzf Python-3.5.6.tgz
    sudo rm Python-3.5.6.tgz
    cd Python-3.5.6
    sudo ./configure --enable-optimizations
    sudo make altinstall
    sudo python3.5 -m pip install --upgrade pip
    sudo python3.5 -m pip install --upgrade setuptools

    echo '--- INSTALANDO NUMPY PARA O PYTHON3.5 ---'
    sudo python3.5 -m pip install numpy
    
    echo '--- CLONANDO O OPENCV 3.4 DO REPOSITÓRIO OFICIAL ---'
    cd /media/sdcard/
    sudo git clone -b 3.4 https://github.com/opencv/opencv.git
    sudo git clone -b 3.4 https://github.com/opencv/opencv_contrib.git
    
    echo '--- COMPILANDO O OPENCV 3.4 ---'
    cd opencv
    sudo mkdir build
    cd build
    sudo cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D BUILD_opencv_java=OFF -D BUILD_opencv_python=OFF -D BUILD_opencv_python3=ON -D PYTHON3_DEFAULT_EXECUTABLE=$(which python3.5) -D PYTHON3_EXECUTABLE:FILEPATH=$(which python3.5) -D PYTHON_INCLUDE_DIR=/usr/local/include/python3.5m/ -D INSTALL_C_EXAMPLES=OFF -D INSTALL_PYTHON3_EXAMPLES=OFF -D BUILD_EXAMPLES=OFF -D WITH_CUDA=OFF -D BUILD_TESTS=OFF -D ENABLE_NEON=ON -D BUILD_PERF_TESTS= OFF -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules ..
    sudo make -j 1
    
    echo '--- INSTALANDO O OPENCV 3.4 ---'
    sudo make install
    
    echo '--- OPENCV INSTALADO ---'
fi
